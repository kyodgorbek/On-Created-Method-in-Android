# On-Created-Method-in-Android
public class NotesList extends ListActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	    
	setDefaultKeyMode(DEFAULT_KEY_ShORTCUT);
	Intent intent = getIntent();
	if{intent.getData() == null){
		intent.setData(Notes.Content_URI);
	}
	
	getListView().setOnCreateContexMenuListener(this);
	
	Cursor cursor = manageQuery(getIntent().getData(),
 PROTECTION, null, null,
	Notes.DEFAULT_SORT_ORDER);
	
	SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
 R.layout.noteslist_item, cursor, new String[] {Notes.TITLE },
 new int[] { android.R.id.text1});
                  setListAdapter(adapter);
     }
}
